#include <stdint.h>
#include <stdio.h>

// Registers
#define RCC_CR             	*((uint32_t*) 0x40021000)
#define RCC_CFGR        	*((uint32_t*) 0x40021004)
#define RCC_AHBENR        	*((uint32_t*) 0x40021014)
#define RCC_APB2ENR			*((uint32_t*) 0x40021018)
#define RCC_APB1ENR        	*((uint32_t*) 0x4002101C)
#define RCC_CFGR2			*((uint32_t*) 0x4002102C)

#define PWR_CR            	*((uint32_t*) 0x40007000)
#define FLASH_ACR        	*((uint32_t*) 0x40022000)

#define GPIOA_MODER       	*((uint32_t*) 0x48000000)
#define GPIOA_OTYPER    	*((uint32_t*) 0x48000004)
#define GPIOA_OSPEEDR    	*((uint32_t*) 0x48000008)
#define GPIOA_PUPDR        	*((uint32_t*) 0x4800000C)
#define GPIOA_BSRR        	*((uint32_t*) 0x48000018)
#define GPIOA_AFRL			*((uint32_t*) 0x48000020)

#define ADC1_ISR			*((uint32_t*) 0x50000000)
#define ADC1_CR 			*((uint32_t*) 0x50000008)
#define ADC1_CCR			*((uint32_t*) 0x50000308)
#define ADC1_CFGR			*((uint32_t*) 0x5000000C)
#define ADC1_SMPR1			*((uint32_t*) 0x50000014)
#define ADC1_SQR1			*((uint32_t*) 0x50000030)
#define ADC1_DR				*((uint32_t*) 0x50000040)

#define USART2_CR1			*((uint32_t*) 0x40004400)
#define USART2_BRR			*((uint32_t*) 0x4000440C)
#define USART2_ISR			*((uint32_t*) 0x4000441C)
#define USART2_TDR			*((uint32_t*) 0x40004428)


// Main clocks
void ClockSetup(void);

// Usart functions
void UartSetup(void);
void SendChar(char c);
void SendString(char* string);

void AdcSetup(void);
uint16_t AdcRead(void);



int main(void)
{
	ClockSetup();
	UartSetup();
	AdcSetup();

    // GPIO configuration - We are using PA8 for our LED
    RCC_AHBENR |= (1 << 17); // Enable GPIOA clock
    GPIOA_MODER |= (0b01 << 16); // Set PA8 as General Purpose Output

    /* Loop forever */
    while(1) {
    	uint16_t adc_reading = AdcRead();

    	char message[200];
    	sprintf(message, "ADC reading: %d\n\r", adc_reading);
    	SendString(message);
    	//for(uint64_t i = 0; i < 1000000; i++);
    }


    // Loop for GPIO
    while(1) {
        GPIOA_BSRR |= (1 << 8); // Set pin
        for(uint64_t i = 0; i < 1000000; i++);
        GPIOA_BSRR |= (1 << 8) << 16; // Unset pin
        for(uint64_t i = 0; i < 1000000; i++);
    }
}

void AdcSetup(void) {
	RCC_AHBENR |= (1 << 17); // Enable GPIOA clock
	RCC_AHBENR |= (1 << 28); // Enable ADC1 clock

	RCC_CFGR2 |= 0b100000000; // Set ADC pre-scaler
	GPIOA_MODER |= (0b11 << 0); // Set PA0 into Analog Mode

	ADC1_SQR1 |= (0b0001 << 6); // ADC has 4 sequencers, this is a complex topic, we just use the first one with the bare minimum

	return;
}

uint16_t AdcRead(void) {
    // Start ADC, wait for it to be enabled
	ADC1_CR |= 1;
    while((ADC1_ISR & 1) == 0);

    // Start conversion, wait for it to finish
    ADC1_CR |= (1 << 2);
    while((ADC1_ISR & (1 << 2)) == 0);

    return ADC1_DR;
}

void SendChar(char c) {
	USART2_TDR = c;
	while((USART2_ISR & (1 << 7)) == 0);
}

void SendString(char* string) {
	for (uint64_t i = 0; string[i] != 0; i++) {
		SendChar(string[i]);
	}
}

// Setup clocks, ugly function for cleaning up main()
void ClockSetup() {

	// Setup clocks
	RCC_CR |= (1 << 0); // Enable HSI
	while((RCC_CR & (1 << 1)) == 0); // Wait for HSI to be ready

	// Configure Power Clock
	RCC_APB1ENR |= (1 << 28);
	PWR_CR |= (3 << 5); // This may be wrong

	// Configure flash interface
	FLASH_ACR |= 0b010; // Use this for >48MHz CPU clock
	FLASH_ACR |= (1 << 4); // Enable pre-fetch buffer
	while((FLASH_ACR & (1 << 5)) == 0); // Wait for pre-fetch buffer to start

	// Set pre-scalers
	RCC_CFGR &= ~(0b1111 << 4); // Set HPRE to 0000, meaning main clock pre-scaler is 1.
	RCC_CFGR &= ~(0b011 << 8);    // Set APB1 pre-scaler to 2
	RCC_CFGR &= ~(0b111 << 11); // Set APB2 pre-scaler to 1

	// Setup PLL clock
	RCC_CFGR |= (14 << 18); // Set PLL multiplier to x16
	RCC_CFGR &= ~(1 << 16); // Set PLL source to HSI
	RCC_CR |= (1 << 24); // Turn PLL on
	while((RCC_CR & (1 << 25)) == 0); // Wait for PLL to start

	// Select clock source
	RCC_CFGR |= (0b10 << 0); // Set PLL_P as the system clock
	while ((RCC_CFGR & (0b10 << 2)) == 0); // Wait for system clock to be set
}

void UartSetup(void) {
	RCC_APB1ENR |= (1<<17);  // Enable UART2 CLOCK
	RCC_AHBENR |= (1 << 17); // Enable GPIOA clock

	GPIOA_MODER |= (2<<4);  // Bits (5:4)= 1:0 --> Alternate Function for Pin PA2
	GPIOA_AFRL |= (7<<8);  // Bytes (11:10:9:8) = 0:1:1:1  --> AF7 Alternate function for USART2 at Pin PA2

	USART2_BRR = (5<<0) | (35<<4); // set pre-scaler

	USART2_CR1 |= (1 << 0);  // UE = 1... Enable USART
	USART2_CR1 |= (1<<3);  // TE=1.. Enable Transmitter
}
