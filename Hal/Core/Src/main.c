/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2022 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <stdio.h>
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
#define RCC_CR             	*((uint32_t*) 0x40021000)
#define RCC_CFGR        	*((uint32_t*) 0x40021004)
#define RCC_AHBENR        	*((uint32_t*) 0x40021014)
#define RCC_APB2ENR			*((uint32_t*) 0x40021018)
#define RCC_APB1ENR        	*((uint32_t*) 0x4002101C)
#define RCC_CFGR2			*((uint32_t*) 0x4002102C)

#define PWR_CR            	*((uint32_t*) 0x40007000)
#define FLASH_ACR        	*((uint32_t*) 0x40022000)

#define GPIOA_MODER       	*((uint32_t*) 0x48000000)
#define GPIOA_OTYPER    	*((uint32_t*) 0x48000004)
#define GPIOA_OSPEEDR    	*((uint32_t*) 0x48000008)
#define GPIOA_PUPDR        	*((uint32_t*) 0x4800000C)
#define GPIOA_BSRR        	*((uint32_t*) 0x48000018)
#define GPIOA_AFRL			*((uint32_t*) 0x48000020)

#define ADC1_ISR			*((uint32_t*) 0x50000000)
#define ADC1_CR 			*((uint32_t*) 0x50000008)
#define ADC1_CCR			*((uint32_t*) 0x50000308)
#define ADC1_CFGR			*((uint32_t*) 0x5000000C)
#define ADC1_SMPR1			*((uint32_t*) 0x50000014)
#define ADC1_SQR1			*((uint32_t*) 0x50000030)
#define ADC1_DR				*((uint32_t*) 0x50000040)

#define USART2_CR1			*((uint32_t*) 0x40004400)
#define USART2_BRR			*((uint32_t*) 0x4000440C)
#define USART2_ISR			*((uint32_t*) 0x4000441C)
#define USART2_TDR			*((uint32_t*) 0x40004428)
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */
void ClockSetup(void);

void UartSetup(void);
void SendChar(char c);
void SendString(char* string);

void AdcSetup(void);
uint16_t AdcRead(void);
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */

  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  /* USER CODE BEGIN 2 */
  //ClockSetup();
  UartSetup();
  AdcSetup();

  // GPIO configuration - We are using PA8 for our LED
  RCC_AHBENR |= (1 << 17); // Enable GPIOA clock
  GPIOA_MODER |= (0b01 << 16); // Set PA8 as General Purpose Output
  GPIOA_BSRR |= (1 << 8); // Set pin, makes LED light up
  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    uint16_t adc_reading = AdcRead();

	char message[200];
	sprintf(message, "ADC reading: %d\n\r", adc_reading);
	SendString(message);
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */
void AdcSetup(void) {
	RCC_AHBENR |= (1 << 17); // Enable GPIOA clock
	RCC_AHBENR |= (1 << 28); // Enable ADC1 clock

	RCC_CFGR2 |= 0b100000000; // Set ADC pre-scaler
	GPIOA_MODER |= (0b11 << 0); // Set PA0 into Analog Mode

	ADC1_SQR1 |= (0b0001 << 6); // ADC has 4 sequencers, this is a complex topic, we just use the first one with the bare minimum

	return;
}

uint16_t AdcRead(void) {
    // Start ADC, wait for it to be enabled
	ADC1_CR |= 1;
    while((ADC1_ISR & 1) == 0);

    // Start conversion, wait for it to finish
    ADC1_CR |= (1 << 2);
    while((ADC1_ISR & (1 << 2)) == 0);

    return ADC1_DR;
}

void SendChar(char c) {
	USART2_TDR = c;
	while((USART2_ISR & (1 << 7)) == 0);
}

void SendString(char* string) {
	for (uint64_t i = 0; string[i] != 0; i++) {
		SendChar(string[i]);
	}
}

// Setup clocks, ugly function for cleaning up main()
void ClockSetup() {

	// Setup clocks
	RCC_CR |= (1 << 0); // Enable HSI
	while((RCC_CR & (1 << 1)) == 0); // Wait for HSI to be ready

	// Configure Power Clock
	RCC_APB1ENR |= (1 << 28);
	PWR_CR |= (3 << 5); // This may be wrong

	// Configure flash interface
	FLASH_ACR |= 0b010; // Use this for >48MHz CPU clock
	FLASH_ACR |= (1 << 4); // Enable pre-fetch buffer
	while((FLASH_ACR & (1 << 5)) == 0); // Wait for pre-fetch buffer to start

	// Set pre-scalers
	RCC_CFGR &= ~(0b1111 << 4); // Set HPRE to 0000, meaning main clock pre-scaler is 1.
	RCC_CFGR &= ~(0b011 << 8);    // Set APB1 pre-scaler to 2
	RCC_CFGR &= ~(0b111 << 11); // Set APB2 pre-scaler to 1

	// Setup PLL clock
	RCC_CFGR |= (14 << 18); // Set PLL multiplier to x16
	RCC_CFGR &= ~(1 << 16); // Set PLL source to HSI
	RCC_CR |= (1 << 24); // Turn PLL on
	while((RCC_CR & (1 << 25)) == 0); // Wait for PLL to start

	// Select clock source
	RCC_CFGR |= (0b10 << 0); // Set PLL_P as the system clock
	while ((RCC_CFGR & (0b10 << 2)) == 0); // Wait for system clock to be set
}

void UartSetup(void) {
	RCC_APB1ENR |= (1<<17);  // Enable UART2 CLOCK
	RCC_AHBENR |= (1 << 17); // Enable GPIOA clock

	GPIOA_MODER |= (2<<4);  // Bits (5:4)= 1:0 --> Alternate Function for Pin PA2
	GPIOA_AFRL |= (7<<8);  // Bytes (11:10:9:8) = 0:1:1:1  --> AF7 Alternate function for USART2 at Pin PA2

	USART2_BRR = (5<<0) | (17<<4); // set pre-scaler, adjusted for auto-generated HAL

	USART2_CR1 |= (1 << 0);  // UE = 1... Enable USART
	USART2_CR1 |= (1<<3);  // TE=1.. Enable Transmitter
}
/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
