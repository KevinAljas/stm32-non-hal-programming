This project is intended for use in giving a crash-course in teaching people the very basics of programming micro-controllers without the provided libraries.

Useful links:
STM32 F303K8 MCU register datasheet: https://www.st.com/resource/en/reference_manual/rm0316-stm32f303xbcde-stm32f303x68-stm32f328x8-stm32f358xc-stm32f398xe-advanced-armbased-mcus-stmicroelectronics.pdf

STM32 Arm M4 MCU datasheet: https://www.st.com/resource/en/datasheet/stm32f303k8.pdf
Used for the "Tabel 14. Alternate functions".

Followed guides:
GPIO register setup: https://controllerstech.com/stm32-gpio-output-config-using-registers/ 
UART register setup: https://controllerstech.com/how-to-setup-uart-using-registers-in-stm32/

